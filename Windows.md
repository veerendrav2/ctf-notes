#### Samba Client CLI
```
# Connect to samba host and list
smbclient -L \\10.10.10.169

# Mount samba directory
mount -t cifs \\10.10.10.169 /mnt/smb

```